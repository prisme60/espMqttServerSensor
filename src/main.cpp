#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ping.h>

// initial time (possibly given by an external RTC)
#define RTC_UTC_TEST 159600000 // 1510592825 = Monday 13 November 2017 17:07:05 UTC


// This database is autogenerated from IANA timezone database
//    https://www.iana.org/time-zones
// and can be updated on demand in this repository
#include <TZ.h>

// "TZ_" macros follow DST change across seasons without source code change
// check for your nearest city in TZ.h

// espressif headquarter TZ
//#define MYTZ TZ_Asia_Shanghai

// example for "Not Only Whole Hours" timezones:
// Kolkata/Calcutta is shifted by 30mn
//#define MYTZ TZ_Asia_Kolkata

// example of a timezone with a variable Daylight-Saving-Time:
// demo: watch automatic time adjustment on Summer/Winter change (DST)
#define MYTZ TZ_Europe_Paris

#include <coredecls.h>                  // settimeofday_cb()
#include <Schedule.h>
#include <PolledTimeout.h>

#include <time.h>                       // time() ctime()
#include <sys/time.h>                   // struct timeval

#include <sntp.h>                       // sntp_servermode_dhcp()

#include <Wire.h>
#include <SPI.h>
#include <Adafruit_BMP280.h>

#include "myMqttBroker.h"
#include "heaterControl.h"
#include "heaterConfig.h"
#include "watchdog.h"
#include "configWifi.h"
#include "configMqtt.h"

static myMQTTBroker myBroker;

// NTP
const char sunday[] PROGMEM = "Dimanche";
const char monday[] PROGMEM = "Lundi";
const char tuesday[] PROGMEM = "Mardi";
const char wednesday[] PROGMEM = "Mercredi";
const char thursday[] PROGMEM = "Jeudi";
const char friday[] PROGMEM = "Vendredi";
const char saturday[] PROGMEM = "Samedi";
const char* const daysOfTheWeek[] PROGMEM = { sunday, monday, tuesday, wednesday, thursday, friday, saturday };
static time_t now = RTC_UTC_TEST;
static esp8266::polledTimeout::periodicMs showTimeNow(15000);
static esp8266::polledTimeout::periodicMs checkConnectionTimer(30000);
static esp8266::polledTimeout::periodicMs updateBoilerTimer(30000);
static esp8266::polledTimeout::periodicMs updateHeaterTimer(30000);
static esp8266::polledTimeout::periodicMs pingVerification(20000);
static esp8266::polledTimeout::periodicMs pingWatchdog(60000);

static float temperature = 0;
static float pressure = 0;

// Because of enclosing box, temperature is a bit higher. We ask to remove 2.0°C from sensor temperature read
constexpr float PROGMEM ENCLOSING_CASE_TEMPERATURE_CORRECTION_OFFSET = -2.0;

static String msgMqtt;

static Adafruit_BMP280 bmp; // I2C


// PING

void ICACHE_FLASH_ATTR
user_ping_recv(void *arg, void *pdata)
{
    struct ping_resp *ping_resp = static_cast<struct ping_resp *>(pdata);
    //struct ping_option *ping_opt = static_cast<struct ping_option *>(arg);

    if (ping_resp->ping_err == -1)
    {
        Serial.println(F("ping host fail"));
    }
    else
    {
        pingWatchdog.reset();
        Serial.print(F("ping recv: byte = "));
        Serial.print(ping_resp->bytes);
        Serial.print(F(", time = "));
        Serial.print(ping_resp->resp_time);
        Serial.println(F(" ms"));
    }
}

void ICACHE_FLASH_ATTR
user_ping_sent(void *arg, void *pdata)
{
    Serial.println(F("user ping finish"));
}

ping_option ping_opt = {
    4, // uint32 count;
    IPAddress {192, 168, 1, 1},// uint32 ip;
    1,// uint32 coarse_time;
    user_ping_recv,// ping_recv_function recv_function;
    user_ping_sent,// ping_sent_function sent_function;
    nullptr,// void* reverse;
};

/*
 * Your WiFi config here
 */
static constexpr bool WiFiAP = false;  // Do yo want the ESP as AP?

ADC_MODE(ADC_VCC) // Read Vcc voltage

/*
 * WiFi init stuff
 */
void startWiFiClient()
{
    Serial.print(F("Connecting to "));
    Serial.println(ssid);
    WiFi.persistent(false);
    WiFi.mode(WIFI_STA);
    WiFi.begin(ssid, pass);

    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(F("."));
    }
    Serial.println();

    Serial.println(F("WiFi connected"));
    Serial.print(F("IP address: "));
    Serial.println(WiFi.localIP().toString());
}

void startWiFiAP()
{
    WiFi.mode(WIFI_AP);
    WiFi.softAP(ssid, pass);
    Serial.println(F("AP started"));
    Serial.print(F("IP address: "));
    Serial.println(WiFi.localIP().toString());
}

void setPower(const String &topic, bool boolPowerState)
{
    myBroker.publish(topic, boolPowerState?F("ON"):F("OFF"));
}

void updateMqttBoiler()
{
    if(HeaterConfig::activeBoilerNeedUpdate() && updateBoilerTimer)
    {
        setPower(TASMOTA_TOPIC_BOILER, HeaterConfig::getActiveBoiler());
        HeaterConfig::boilerUpdated();
    }
}

void updateMqttHeater()
{
    if(HeaterConfig::activeHeaterNeedUpdate() || updateHeaterTimer)
    {
        setPower(TASMOTA_TOPIC_HEATER, HeaterConfig::getActiveHeater());
        HeaterConfig::heaterUpdated();
    }
}

void setup_bmp280()
{
  Wire.begin(D2 ,D1);
  if (!bmp.begin(0x76))
  {
    Serial.println(F("Could not find a valid BMP280 sensor, check wiring!"));
    while (1);
  }

  /* Default settings from datasheet. */
  bmp.setSampling(Adafruit_BMP280::MODE_NORMAL,     /* Operating Mode. */
                  Adafruit_BMP280::SAMPLING_X16,    /* Temp. oversampling */
                  Adafruit_BMP280::SAMPLING_X16,    /* Pressure oversampling */
                  Adafruit_BMP280::FILTER_X16,      /* Filtering. */
                  Adafruit_BMP280::STANDBY_MS_500); /* Standby time. */
}

void setup()
{
    Serial.begin(115200);
    Serial.println();
    Serial.println();

    configTime(MYTZ, "fr.pool.ntp.org");

    // Start WiFi
    if (WiFiAP)
        startWiFiAP();
    else
        startWiFiClient();

    // Start the broker
    Serial.println(F("Starting MQTT broker"));
    myBroker.init();

    setup_bmp280();

    msgMqtt.reserve(128);
    myBroker.subscribe(TASMOTA_STATE);
    lwdInit();
}

void loop()
{
    lwdtFeed(); // should be first instruction in loop()

    if(checkConnectionTimer)
    {
        switch(WiFi.status())
        {
            case WL_CONNECTION_LOST:
            case WL_DISCONNECTED:
                Serial.println(F("Wifi disconnected, try to reconnect !"));
                WiFi.reconnect();
                break;
            case WL_CONNECTED:
                Serial.println(F("Wifi connected!!!"));
                if(pingVerification)
                {
                    Serial.println(F("ping_start()"));
                    ping_start(&ping_opt);
                }
                break;
            case WL_NO_SSID_AVAIL:
            case WL_CONNECT_FAILED:
            case WL_IDLE_STATUS:
            case WL_NO_SHIELD:
            case WL_SCAN_COMPLETED:
                break;
        }
    }
    if(pingWatchdog)
    {
        Serial.println(F("============> Restart Board, because of ping failure!!!"));
        ESP.restart();
    }

    // do anything here
    if(showTimeNow)
    {
        lwdtStamp(Where::TIME_MODULE);
        now = time(nullptr);
        Serial.print(F("local :"));
        Serial.print(ctime(&now));
        Serial.print(F("NOW :"));
        Serial.println(now);
        if(now > RTC_UTC_TEST) // "now" is a valid date
        {
            Serial.println(F("## TIME IS VALID###"));
    #if LWIP_VERSION_MAJOR == 1 // Offset of 8 hours when using LWIP 1.4
            now -= 8 * 60 * 60;
    #endif
            lwdtStamp(Where::TIME_MQTT_MODULE);
            tm tm;
            if(localtime_r(&now, &tm) != nullptr)
            {
                msgMqtt = daysOfTheWeek[tm.tm_wday];
                msgMqtt += F(" ");
                msgMqtt += tm.tm_mday;
                msgMqtt += F("-");
                msgMqtt += (tm.tm_mon + 1);
                msgMqtt += F("-");
                msgMqtt += (1900 + tm.tm_year);
                msgMqtt += F(" ");
                msgMqtt += tm.tm_hour;
                msgMqtt += F(":");
                msgMqtt += tm.tm_min;
                msgMqtt += F(":");
                msgMqtt += tm.tm_sec;

                myBroker.publish(F("stat/" DEV_NAME "/datetime"), msgMqtt);

                HeaterControl::runTrigger(tm.tm_wday,tm.tm_hour,tm.tm_min);
            }
        }

        lwdtStamp(Where::SENSOR_MODULE);
        temperature = bmp.readTemperature() + ENCLOSING_CASE_TEMPERATURE_CORRECTION_OFFSET;
        pressure = bmp.readPressure();

        lwdtStamp(Where::SENSOR_MQTT_MODULE);
        msgMqtt = F("{ \"idx\" : 2, \"nvalue\" : 0, \"svalue\" : \"");
        msgMqtt += temperature;
        msgMqtt += F(";");
        msgMqtt += pressure / 100;
        msgMqtt += F(";0;0\"}");
        Serial.println(msgMqtt);
        myBroker.publish(F("domoticz/in"), msgMqtt);
        myBroker.publish(TEMPERATURE, String(temperature));
        myBroker.publish(PRESSURE, String(pressure / 100));

        HeaterControl::adaptTemperature(temperature * 10);

        updateMqttHeater();
        updateMqttBoiler();
    }
    lwdtStamp(Where::LOOP_START);
}
