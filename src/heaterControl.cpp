#include <Arduino.h>

#include "heaterControl.h"
#include "heaterConfig.h"

//            "SMTWTFS", h, m, target
const std::array<const TimerItem, 8> timers ={
    TimerItem("0111110", 6, 0, Target::Comfort),   // Monday to friday
    TimerItem("0111110", 21, 0, Target::Economy),  // Monday to friday
    TimerItem("0000001", 8, 0, Target::Comfort),   // Saturday
    TimerItem("0000001", 21, 30, Target::Economy), // Saturday
    TimerItem("1000000", 6, 0, Target::Comfort),   // Sunday
    TimerItem("1000000", 7, 30, Target::Economy),  // Sunday
    TimerItem("1000000", 10, 15, Target::Comfort), // Sunday
    TimerItem("1000000", 21, 30, Target::Economy), // Sunday
};

const TimerItem * HeaterControl::searchTrigger(int day, int hour, int minute, const std::vector<Target> &targets)
{
    TimerItem const * nearestBeforeTimerItem  = nullptr;
    int nbMinutesBeforeOfNearestTimer =  24 * 60 * 60;
    for (const auto& timerItem : timers)
    {
        if (timerItem.getDays()[day] && std::find(targets.begin(), targets.end(), timerItem.getTarget()) != targets.end())
        {
            int nbMinutes = (hour - timerItem.getHours()) * 60 + minute - timerItem.getMinutes();
            if (nbMinutes >= 0 && nbMinutes < nbMinutesBeforeOfNearestTimer)
            {
                nearestBeforeTimerItem = &timerItem;
                nbMinutesBeforeOfNearestTimer = nbMinutes;
            }
        }
    }
    return nearestBeforeTimerItem;
}

//The method should be called each minute (prefer every < 59 seconds to be sure to not miss timer trig!)
void HeaterControl::runTrigger(int day, int hour, int minute)
{
    if(HeaterConfig::activeBoilerNeedUpdate())
    {
        Serial.print(F("Before : Boiler : Need update => "));
        Serial.println(HeaterConfig::getActiveBoiler()?F("ON"):F("OFF"));
    }
    auto nearestBeforeTimerItemComfort = HeaterControl::searchTrigger(day, hour, minute, {Target::Comfort, Target::Economy});
    if(nearestBeforeTimerItemComfort != nullptr)
    {
        switch (nearestBeforeTimerItemComfort->getTarget())
        {
        case Target::Comfort:
            HeaterConfig::setcomfortPeriod(true);
            break;
        case Target::Economy:
            HeaterConfig::setcomfortPeriod(false);
            break;
        case Target::Boiler_ON:
        case Target::Boiler_OFF:
            // Should not occur!
            break;
        }
    }
    auto nearestBeforeTimerItemBoiler = HeaterControl::searchTrigger(day, hour, minute, {Target::Boiler_ON, Target::Boiler_OFF});
    if(nearestBeforeTimerItemBoiler != nullptr)
    {
        switch (nearestBeforeTimerItemBoiler->getTarget())
        {
        case Target::Boiler_ON:
            HeaterConfig::setActiveBoiler(true);
            break;
        case Target::Boiler_OFF:
            HeaterConfig::setActiveBoiler(false);
            break;
        case Target::Comfort:
        case Target::Economy:
            // Should not occur!
            break;
        }
    }
    if(HeaterConfig::activeBoilerNeedUpdate())
    {
        Serial.print(F("After : Boiler : Need update => "));
        Serial.println(HeaterConfig::getActiveBoiler()?F("ON"):F("OFF"));
    }
}

void HeaterControl::adaptTemperature(int sensorTemperature)
{
    if(HeaterConfig::activeHeaterNeedUpdate())
    {
        Serial.print(F("After : Heater : Need update => "));
        Serial.println(HeaterConfig::getActiveHeater()?F("ON"):F("OFF"));
    }
    if(HeaterConfig::iscomfortPeriod())
    {
        if(sensorTemperature > HeaterConfig::getcomfortTemperature() && HeaterConfig::getActiveHeater())
        {
            HeaterConfig::setActiveHeater(false);
            if(HeaterConfig::activeHeaterNeedUpdate())
            {
                Serial.println(F("CONFORT : Chauffage ON"));
            }
        }
        else if(sensorTemperature < (HeaterConfig::getcomfortTemperature() - HeaterConfig::getHysterisisTemperature()) && !HeaterConfig::getActiveHeater())
        {
            HeaterConfig::setActiveHeater(true);
            if(HeaterConfig::activeHeaterNeedUpdate())
            {
                Serial.println(F("CONFORT : Chauffage OFF"));
            }
        }
    }
    else // Economy
    {
        if(sensorTemperature > HeaterConfig::getEconomyTemperature() && HeaterConfig::getActiveHeater())
        {
            HeaterConfig::setActiveHeater(false);
            if(HeaterConfig::activeHeaterNeedUpdate())
            {
                Serial.println(F("ECONOMIE : Chauffage OFF"));
            }
        }
        else if(sensorTemperature < (HeaterConfig::getEconomyTemperature() - HeaterConfig::getHysterisisTemperature()) && !HeaterConfig::getActiveHeater())
        {
            HeaterConfig::setActiveHeater(true);
            if(HeaterConfig::activeHeaterNeedUpdate())
            {
                Serial.println(F("ECONOMIE : Chauffage OFF"));
            }
        }
    }
    if(HeaterConfig::getActiveHeater() && !HeaterConfig::getActiveBoiler())
    {
        HeaterConfig::setActiveBoiler(true);
        if(HeaterConfig::activeBoilerNeedUpdate())
        {
            Serial.println(F("Chauffage ON => Chaudiere ON"));
        }
    }
    if(HeaterConfig::activeHeaterNeedUpdate())
    {
        Serial.print(F("After : Heater : Need update => "));
        Serial.println(HeaterConfig::getActiveHeater()?F("ON"):F("OFF"));
    }
}