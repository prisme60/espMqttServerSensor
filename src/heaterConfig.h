#pragma once

template<typename T>
class TrackingValue
{
    private:
        T val = T();
        bool valNeedUpdate = false;

    public:
        void setVal(bool val)
        {
            if(val != this->val)
            {
                this->val = val;
                valNeedUpdate = true;
            }
        }

        constexpr T getVal() { return val; }
        constexpr bool needUpdate() { return valNeedUpdate; }
        void updated() { valNeedUpdate = false; }
};

class HeaterConfig
{
    public:
        static constexpr bool getActiveBoiler() { return HeaterConfig::activeBoiler.getVal(); }
        static void setActiveBoiler(bool activeBoiler) { return HeaterConfig::activeBoiler.setVal(activeBoiler); }
        static constexpr bool activeBoilerNeedUpdate() { return HeaterConfig::activeBoiler.needUpdate(); }
        static void boilerUpdated() { activeBoiler.updated(); }

        static constexpr bool getActiveHeater() { return activeHeater.getVal(); }
        static void setActiveHeater(bool activeHeater) { return HeaterConfig::activeHeater.setVal(activeHeater); }
        static constexpr bool activeHeaterNeedUpdate() { return activeHeater.needUpdate(); }
        static void heaterUpdated() { activeHeater.updated(); }

        static bool iscomfortPeriod() { return HeaterConfig::comfortPeriod; }
        static void setcomfortPeriod(bool comfortPeriodtoSet) { HeaterConfig::comfortPeriod = comfortPeriodtoSet; }

        // Comfort temperature (unit : 0.1°C)
        static int getcomfortTemperature() { return HeaterConfig::comfortTemperature; }
        // Economy temperature (unit : 0.1°C)
        static int getEconomyTemperature() { return HeaterConfig::economyTemperature; }
        // Hysteris (unit : 0.1°C)
        static int getHysterisisTemperature() { return HeaterConfig::hysterisisTemperature; }

    private:
        // Indicate if we are in a comfort period (true) or in economy period (false)
        static bool comfortPeriod;
        // Indicate if boiler is active (true) or stopped (false)
        static TrackingValue<bool> activeBoiler;
        // Indicate if heater is active (true) or stopped (false)
        static TrackingValue<bool> activeHeater;

        // Comfort temperature (unit : 0.1°C)
        static int comfortTemperature;
        // Economy temperature (unit : 0.1°C)
        static int economyTemperature;
        // Hysteris (unit : 0.1°C)
        static int hysterisisTemperature;
};