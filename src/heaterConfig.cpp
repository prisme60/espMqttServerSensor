#include "heaterConfig.h"

bool HeaterConfig::comfortPeriod = false;
TrackingValue<bool> HeaterConfig::activeHeater;
TrackingValue<bool> HeaterConfig::activeBoiler;

int HeaterConfig::comfortTemperature = 190;
int HeaterConfig::economyTemperature = 165;
int HeaterConfig::hysterisisTemperature = 1;
