#include <Arduino.h>

#include "myMqttBroker.h"
#include "configMqtt.h"
#include "heaterConfig.h"

bool myMQTTBroker::onConnect(IPAddress addr, uint16_t client_count)
{
    Serial.print(addr.toString());
    Serial.print(F(" connected, client_count = "));
    Serial.println(client_count);
    return true;
}

bool myMQTTBroker::onAuth(String username, String password)
{
    Serial.print(F("Username/Password: "));
    Serial.print(username);
    Serial.print(F("/"));
    Serial.println(password);
    return true;
}

void myMQTTBroker::onData(String topic, const char *data, uint32_t length)
{
    char data_str[length+1];
    os_memcpy(data_str, data, length);
    data_str[length] = '\0';
    
    Serial.print(F("Received topic '"));
    Serial.print(topic);
    Serial.print(F("' with data '"));
    Serial.print(data_str);
    Serial.println(F("'"));
    if(topic == TASMOTA_STATE)
    {
        //{"Time":"2020-11-07T15:15:46","Uptime":"13T22:08:09","UptimeSec":1202889,"Heap":27,"SleepMode":"Dynamic","Sleep":50,
        //"LoadAvg":19,"MqttCount":24,"POWER1":"OFF","POWER2":"OFF","POWER3":"OFF","POWER4":"OFF",
        //"Wifi":{"AP":1,"SSId":"WIFI_SSID","BSSId":"60:35:C0:64:48:16","Channel":1,"RSSI":50,"Signal":-75,"LinkCount":1,"Downtime":"0T00:00:05"}}
        if(strstr(data_str,"\"POWER1\":\"OFF\"") != nullptr)
        {
            Serial.println(F("HeaterConfig::setActiveBoiler(false)"));
            HeaterConfig::setActiveBoiler(false);
        }
        else if(strstr(data_str,"\"POWER1\":\"ON\"") != nullptr)
        {
            Serial.println(F("HeaterConfig::setActiveBoiler(true)"));
            HeaterConfig::setActiveBoiler(true);
        }
    }
}