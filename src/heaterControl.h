#pragma once

#include <bitset>
#include <array>

constexpr int DAYS_BY_WEEK = 7;

using Days = std::bitset<DAYS_BY_WEEK>;

enum class Target
{
    Comfort,
    Economy,
    Boiler_ON,
    Boiler_OFF,
};

using Time = struct Time_
{
    int hours;
    int minutes;
};

class TimerItem
{
    public:
    TimerItem(const char days[], int hours, int minutes, Target target)
    : days(days), time({hours, minutes}), target(target) {}
    private:
        const Days days;
        const Time time;
        const Target target;
    public:
        const Days& getDays() const {return days;}
        const Time& getTime() const {return time;}
        const int getHours() const {return time.hours;}
        const int getMinutes() const {return time.minutes;}
        const Target& getTarget() const {return target;}
};

class HeaterControl
{
    public:
    static void runTrigger(int day, int hour, int minute);
    static void adaptTemperature(int temperature);

    private:
    static const TimerItem * searchTrigger(int day, int hour, int minute, const std::vector<Target> &targets);
};