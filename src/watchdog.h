#define LWD_TIMEOUT       12000  // Restart if loop watchdog timer reaches this time out value (12 seconds)

#define LWD_MARKER        0xABBA0000

enum class Where
{
    LOOP_START,
    LWD_OVERWRITTEN,
    TIME_MODULE,
    TIME_MQTT_MODULE,
    SENSOR_MODULE,
    SENSOR_MQTT_MODULE,
};

constexpr unsigned long where(Where whereLocation)
{
    return LWD_MARKER | static_cast<unsigned long>(whereLocation);
}

void lwdInit();
void lwdRestart();
void lwdtStamp(Where where);
void lwdtFeed();