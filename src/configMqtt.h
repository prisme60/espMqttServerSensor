#pragma once

#define DEV_NAME "esp8266"

#define TASMOTA_TOPIC_BOILER F("cmnd/tasmota/POWER1")
#define TASMOTA_TOPIC_HEATER F("cmnd/tasmota/POWER3")

#define TASMOTA_STATE F("tele/tasmota/STATE")

#define TEMPERATURE F("stat/" DEV_NAME "/temp")
#define PRESSURE    F("stat/" DEV_NAME "/pres")
