#include <Ticker.h>
#include <Arduino.h>
#include <ESP8266WiFi.h>
#include "watchdog.h"
#include "heaterConfig.h"

const char moduleName0[] PROGMEM = "start of loop()";
const char moduleName1[] PROGMEM = "lwd variables overwritten";
const char moduleName2[] PROGMEM = "time module";
const char moduleName3[] PROGMEM = "sensor module()";
const char* const moduleNames[] PROGMEM = {
  moduleName0,
  moduleName1,
  moduleName2,
  moduleName3
};


Ticker lwdTicker;

volatile unsigned long    lwdTime = 0;
volatile unsigned long    lwdTimeout = LWD_TIMEOUT;
volatile unsigned long    lwdWhere = where(Where::LOOP_START);
volatile bool             lwdIncomplete = false;

static bool isValidWhere()
{
    return (lwdWhere & 0xFFFF0000) == LWD_MARKER;
}

void ICACHE_RAM_ATTR lwdtcb()
{
    Serial.print(F("Pouette lwdTime = "));
    Serial.print(lwdTime);
    Serial.print(F(" lwdTimeout="));
    Serial.print(lwdTimeout);
    Serial.print(F(" Comfort mode :"));
    Serial.print(HeaterConfig::iscomfortPeriod());
    Serial.print(F(" Heater state :"));
    Serial.print(HeaterConfig::getActiveHeater());
    Serial.print(F(" Free Heap :"));
    Serial.print(ESP.getFreeHeap());
    Serial.print(F(" Max Free Block Size :"));
    Serial.print(ESP.getMaxFreeBlockSize());
    Serial.print(F(" Heap Fragmentation :"));
    Serial.print(ESP.getHeapFragmentation());
    Serial.print(F(" CpuFreqMHz :"));
    Serial.print(ESP.getCpuFreqMHz());
    Serial.print(F(" Vcc :"));
    Serial.print(ESP.getVcc());
    Serial.print(F(" Wifi status :"));
    Serial.println(WiFi.status());
  if (lwdTimeout - lwdTime != LWD_TIMEOUT) // time variables out of sync
    lwdWhere = where(Where::LWD_OVERWRITTEN);
  else if (!isValidWhere()) 
    lwdWhere = where(Where::LWD_OVERWRITTEN);           // where variable mangled    
  else if (millis() - lwdTime < LWD_TIMEOUT)
    return;
  lwdRestart(); // lwd timed out or lwd overwritten
}

void lwdInit()
{
    // setup loop watch dog
    lwdTicker.attach_ms(LWD_TIMEOUT / 2, lwdtcb); // attach lwdt interrupt service routine to ticker
    lwdTime = millis();
}

void lwdRestart()
{
  int index = (lwdWhere & 0xFFFF);
  if (lwdIncomplete) 
    Serial.printf_P(PSTR("\n\nLoop watch dog: loop() not completed. Last seen routine: %s\n"), moduleNames[index]);
  else 
    Serial.printf_P(PSTR("\n\nLoop watch dog timed out in routine %s\n"), moduleNames[index]);
  Serial.flush();  
  ESP.restart();
}

void lwdtStamp(Where whereLocation)
{
  lwdWhere = where(whereLocation);
}
   
void lwdtFeed()
{
  lwdTime = millis();
  lwdTimeout = lwdTime + LWD_TIMEOUT;
  lwdIncomplete = (lwdWhere != where(Where::LOOP_START));
  if (lwdIncomplete)
  {
    lwdRestart();
  }
}